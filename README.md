# FIR Docker Compose

## Deployment

### Install depends:
`apt-get install python-pip`
`pip install docker-compose`


## Starting / Configuring FIR

Spin up fir containers for the first time
`docker-compose up -d`

Then
`docker-compose down`

Configure `base.py` settings:
`cd /var/lib/docker/volumes/fir-docker-compose_fir_data/_data/FIR/fir/config/`
`vi base.py`

Start up for good
`docker-compose up -d`


